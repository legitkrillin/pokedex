def colour(self):
    if self.type1.text() == 'Grass' or self.type1.text() == 'Grass +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.type1.text() == 'Electric' or self.type1.text() == 'Electric +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.type1.text() == 'Water' or self.type1.text() == 'Water +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.type1.text() == 'Rock' or self.type1.text() == 'Rock +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.type1.text() == 'Poison' or self.type1.text() == 'Poison +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.type1.text() == 'Ice' or self.type1.text() == 'Ice +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.type1.text() == 'Flying' or self.type1.text() == 'Flying +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.type1.text() == 'Fighting' or self.type1.text() == 'Fighting +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.type1.text() == 'Dark' or self.type1.text() == 'Dark +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.type1.text() == 'Steel' or self.type1.text() == 'Steel +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.type1.text() == 'Psychic' or self.type1.text() == 'Psychic +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.type1.text() == 'Normal' or self.type1.text() == 'Normal +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.type1.text() == 'Ground' or self.type1.text() == 'Ground +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.type1.text() == 'Ghost' or self.type1.text() == 'Ghost +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.type1.text() == 'Fire' or self.type1.text() == 'Fire +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.type1.text() == 'Fairy' or self.type1.text() == 'Fairy +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.type1.text() == 'Dragon' or self.type1.text() == 'Dragon +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.type1.text() == 'Bug' or self.type1.text() == 'Bug +4 Damage':
        self.type1.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.type1.text() == '':
        self.type1.setStyleSheet("")

    if self.type2.text() == 'Grass' or self.type2.text() == 'Grass +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.type2.text() == 'Electric' or self.type2.text() == 'Electric +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.type2.text() == 'Water' or self.type2.text() == 'Water +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.type2.text() == 'Rock' or self.type2.text() == 'Rock +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.type2.text() == 'Poison' or self.type2.text() == 'Poison +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.type2.text() == 'Ice' or self.type2.text() == 'Ice +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.type2.text() == 'Flying' or self.type2.text() == 'Flying +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.type2.text() == 'Fighting' or self.type2.text() == 'Fighting +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.type2.text() == 'Dark' or self.type2.text() == 'Dark +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.type2.text() == 'Steel' or self.type2.text() == 'Steel +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.type2.text() == 'Psychic' or self.type2.text() == 'Psychic +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.type2.text() == 'Normal' or self.type2.text() == 'Normal +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.type2.text() == 'Ground' or self.type2.text() == 'Ground +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.type2.text() == 'Ghost' or self.type2.text() == 'Ghost +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.type2.text() == 'Fire' or self.type2.text() == 'Fire +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.type2.text() == 'Fairy' or self.type2.text() == 'Fairy +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.type2.text() == 'Dragon' or self.type2.text() == 'Dragon +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.type2.text() == 'Bug' or self.type2.text() == 'Bug +4 Damage':
        self.type2.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.type2.text() == '':
        self.type2.setStyleSheet("")

    if self.weaknesses1.text() == 'Grass' or self.weaknesses1.text() == 'Grass +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.weaknesses1.text() == 'Electric' or self.weaknesses1.text() == 'Electric +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.weaknesses1.text() == 'Water' or self.weaknesses1.text() == 'Water +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.weaknesses1.text() == 'Rock' or self.weaknesses1.text() == 'Rock +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.weaknesses1.text() == 'Poison' or self.weaknesses1.text() == 'Poison +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.weaknesses1.text() == 'Ice' or self.weaknesses1.text() == 'Ice +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.weaknesses1.text() == 'Flying' or self.weaknesses1.text() == 'Flying +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.weaknesses1.text() == 'Fighting' or self.weaknesses1.text() == 'Fighting +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.weaknesses1.text() == 'Dark' or self.weaknesses1.text() == 'Dark +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.weaknesses1.text() == 'Steel' or self.weaknesses1.text() == 'Steel +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.weaknesses1.text() == 'Psychic' or self.weaknesses1.text() == 'Psychic +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.weaknesses1.text() == 'Normal' or self.weaknesses1.text() == 'Normal +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.weaknesses1.text() == 'Ground' or self.weaknesses1.text() == 'Ground +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.weaknesses1.text() == 'Ghost' or self.weaknesses1.text() == 'Ghost +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.weaknesses1.text() == 'Fire' or self.weaknesses1.text() == 'Fire +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.weaknesses1.text() == 'Fairy' or self.weaknesses1.text() == 'Fairy +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.weaknesses1.text() == 'Dragon' or self.weaknesses1.text() == 'Dragon +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.weaknesses1.text() == 'Bug' or self.weaknesses1.text() == 'Bug +4 Damage':
        self.weaknesses1.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.weaknesses1.text() == '':
        self.weaknesses1.setStyleSheet("")

    if self.weaknesses2.text() == 'Grass' or self.weaknesses2.text() == 'Grass +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.weaknesses2.text() == 'Electric' or self.weaknesses2.text() == 'Electric +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.weaknesses2.text() == 'Water' or self.weaknesses2.text() == 'Water +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.weaknesses2.text() == 'Rock' or self.weaknesses2.text() == 'Rock +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.weaknesses2.text() == 'Poison' or self.weaknesses2.text() == 'Poison +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.weaknesses2.text() == 'Ice' or self.weaknesses2.text() == 'Ice +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.weaknesses2.text() == 'Flying' or self.weaknesses2.text() == 'Flying +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.weaknesses2.text() == 'Fighting' or self.weaknesses2.text() == 'Fighting +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.weaknesses2.text() == 'Dark' or self.weaknesses2.text() == 'Dark +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.weaknesses2.text() == 'Steel' or self.weaknesses2.text() == 'Steel +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.weaknesses2.text() == 'Psychic' or self.weaknesses2.text() == 'Psychic +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.weaknesses2.text() == 'Normal' or self.weaknesses2.text() == 'Normal +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.weaknesses2.text() == 'Ground' or self.weaknesses2.text() == 'Ground +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.weaknesses2.text() == 'Ghost' or self.weaknesses2.text() == 'Ghost +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.weaknesses2.text() == 'Fire' or self.weaknesses2.text() == 'Fire +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.weaknesses2.text() == 'Fairy' or self.weaknesses2.text() == 'Fairy +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.weaknesses2.text() == 'Dragon' or self.weaknesses2.text() == 'Dragon +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.weaknesses2.text() == 'Bug' or self.weaknesses2.text() == 'Bug +4 Damage':
        self.weaknesses2.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.weaknesses2.text() == '':
        self.weaknesses2.setStyleSheet("")

    if self.weaknesses3.text() == 'Grass' or self.weaknesses3.text() == 'Grass +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.weaknesses3.text() == 'Electric' or self.weaknesses3.text() == 'Electric +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.weaknesses3.text() == 'Water' or self.weaknesses3.text() == 'Water +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.weaknesses3.text() == 'Rock' or self.weaknesses3.text() == 'Rock +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.weaknesses3.text() == 'Poison' or self.weaknesses3.text() == 'Poison +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.weaknesses3.text() == 'Ice' or self.weaknesses3.text() == 'Ice +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.weaknesses3.text() == 'Flying' or self.weaknesses3.text() == 'Flying +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.weaknesses3.text() == 'Fighting' or self.weaknesses3.text() == 'Fighting +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.weaknesses3.text() == 'Dark' or self.weaknesses3.text() == 'Dark +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.weaknesses3.text() == 'Steel' or self.weaknesses3.text() == 'Steel +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.weaknesses3.text() == 'Psychic' or self.weaknesses3.text() == 'Psychic +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.weaknesses3.text() == 'Normal' or self.weaknesses3.text() == 'Normal +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.weaknesses3.text() == 'Ground' or self.weaknesses3.text() == 'Ground +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.weaknesses3.text() == 'Ghost' or self.weaknesses3.text() == 'Ghost +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.weaknesses3.text() == 'Fire' or self.weaknesses3.text() == 'Fire +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.weaknesses3.text() == 'Fairy' or self.weaknesses3.text() == 'Fairy +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.weaknesses3.text() == 'Dragon' or self.weaknesses3.text() == 'Dragon +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.weaknesses3.text() == 'Bug' or self.weaknesses3.text() == 'Bug +4 Damage':
        self.weaknesses3.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.weaknesses3.text() == '':
        self.weaknesses3.setStyleSheet("")

    if self.weaknesses4.text() == 'Grass' or self.weaknesses4.text() == 'Grass +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.weaknesses4.text() == 'Electric' or self.weaknesses4.text() == 'Electric +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.weaknesses4.text() == 'Water' or self.weaknesses4.text() == 'Water +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.weaknesses4.text() == 'Rock' or self.weaknesses4.text() == 'Rock +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.weaknesses4.text() == 'Poison' or self.weaknesses4.text() == 'Poison +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.weaknesses4.text() == 'Ice' or self.weaknesses4.text() == 'Ice +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.weaknesses4.text() == 'Flying' or self.weaknesses4.text() == 'Flying +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.weaknesses4.text() == 'Fighting' or self.weaknesses4.text() == 'Fighting +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.weaknesses4.text() == 'Dark' or self.weaknesses4.text() == 'Dark +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.weaknesses4.text() == 'Steel' or self.weaknesses4.text() == 'Steel +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.weaknesses4.text() == 'Psychic' or self.weaknesses4.text() == 'Psychic +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.weaknesses4.text() == 'Normal' or self.weaknesses4.text() == 'Normal +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.weaknesses4.text() == 'Ground' or self.weaknesses4.text() == 'Ground +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.weaknesses4.text() == 'Ghost' or self.weaknesses4.text() == 'Ghost +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.weaknesses4.text() == 'Fire' or self.weaknesses4.text() == 'Fire +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.weaknesses4.text() == 'Fairy' or self.weaknesses4.text() == 'Fairy +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.weaknesses4.text() == 'Dragon' or self.weaknesses4.text() == 'Dragon +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.weaknesses4.text() == 'Bug' or self.weaknesses4.text() == 'Bug +4 Damage':
        self.weaknesses4.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.weaknesses4.text() == '':
        self.weaknesses4.setStyleSheet("")

    if self.weaknesses5.text() == 'Grass' or self.weaknesses5.text() == 'Grass +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.weaknesses5.text() == 'Electric' or self.weaknesses5.text() == 'Electric +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.weaknesses5.text() == 'Water' or self.weaknesses5.text() == 'Water +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.weaknesses5.text() == 'Rock' or self.weaknesses5.text() == 'Rock +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.weaknesses5.text() == 'Poison' or self.weaknesses5.text() == 'Poison +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.weaknesses5.text() == 'Ice' or self.weaknesses5.text() == 'Ice +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.weaknesses5.text() == 'Flying' or self.weaknesses5.text() == 'Flying +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.weaknesses5.text() == 'Fighting' or self.weaknesses5.text() == 'Fighting +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.weaknesses5.text() == 'Dark' or self.weaknesses5.text() == 'Dark +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.weaknesses5.text() == 'Steel' or self.weaknesses5.text() == 'Steel +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.weaknesses5.text() == 'Psychic' or self.weaknesses5.text() == 'Psychic +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.weaknesses5.text() == 'Normal' or self.weaknesses5.text() == 'Normal +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.weaknesses5.text() == 'Ground' or self.weaknesses5.text() == 'Ground +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.weaknesses5.text() == 'Ghost' or self.weaknesses5.text() == 'Ghost +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.weaknesses5.text() == 'Fire' or self.weaknesses5.text() == 'Fire +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.weaknesses5.text() == 'Fairy' or self.weaknesses5.text() == 'Fairy +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.weaknesses5.text() == 'Dragon' or self.weaknesses5.text() == 'Dragon +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.weaknesses5.text() == 'Bug' or self.weaknesses5.text() == 'Bug +4 Damage':
        self.weaknesses5.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.weaknesses5.text() == '':
        self.weaknesses5.setStyleSheet("")

    if self.weaknesses6.text() == 'Grass' or self.weaknesses6.text() == 'Grass +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.weaknesses6.text() == 'Electric' or self.weaknesses6.text() == 'Electric +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.weaknesses6.text() == 'Water' or self.weaknesses6.text() == 'Water +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.weaknesses6.text() == 'Rock' or self.weaknesses6.text() == 'Rock +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.weaknesses6.text() == 'Poison' or self.weaknesses6.text() == 'Poison +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.weaknesses6.text() == 'Ice' or self.weaknesses6.text() == 'Ice +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.weaknesses6.text() == 'Flying' or self.weaknesses6.text() == 'Flying +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.weaknesses6.text() == 'Fighting' or self.weaknesses6.text() == 'Fighting +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.weaknesses6.text() == 'Dark' or self.weaknesses6.text() == 'Dark +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.weaknesses6.text() == 'Steel' or self.weaknesses6.text() == 'Steel +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.weaknesses6.text() == 'Psychic' or self.weaknesses6.text() == 'Psychic +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.weaknesses6.text() == 'Normal' or self.weaknesses6.text() == 'Normal +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.weaknesses6.text() == 'Ground' or self.weaknesses6.text() == 'Ground +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.weaknesses6.text() == 'Ghost' or self.weaknesses6.text() == 'Ghost +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.weaknesses6.text() == 'Fire' or self.weaknesses6.text() == 'Fire +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.weaknesses6.text() == 'Fairy' or self.weaknesses6.text() == 'Fairy +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.weaknesses6.text() == 'Dragon' or self.weaknesses6.text() == 'Dragon +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.weaknesses6.text() == 'Bug' or self.weaknesses6.text() == 'Bug +4 Damage':
        self.weaknesses6.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.weaknesses6.text() == '':
        self.weaknesses6.setStyleSheet("")

    if self.weaknesses7.text() == 'Grass' or self.weaknesses7.text() == 'Grass +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: lightgreen; color: black }""")
    if self.weaknesses7.text() == 'Electric' or self.weaknesses7.text() == 'Electric +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #e0e324; color: black }""")
    if self.weaknesses7.text() == 'Water' or self.weaknesses7.text() == 'Water +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #455cde; color: white }""")
    if self.weaknesses7.text() == 'Rock' or self.weaknesses7.text() == 'Rock +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #a39c5f; color: white }""")
    if self.weaknesses7.text() == 'Poison' or self.weaknesses7.text() == 'Poison +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #cc74c9; color: white }""")
    if self.weaknesses7.text() == 'Ice' or self.weaknesses7.text() == 'Ice +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #4eabde; color: white }""")
    if self.weaknesses7.text() == 'Flying' or self.weaknesses7.text() == 'Flying +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #9fd6fc; color: black }""")
    if self.weaknesses7.text() == 'Fighting' or self.weaknesses7.text() == 'Fighting +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #de5216; color: white }""")
    if self.weaknesses7.text() == 'Dark' or self.weaknesses7.text() == 'Dark +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #898a81; color: white }""")
    if self.weaknesses7.text() == 'Steel' or self.weaknesses7.text() == 'Steel +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #6d7a73; color: black }""")
    if self.weaknesses7.text() == 'Psychic' or self.weaknesses7.text() == 'Psychic +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #ff99ff; color: white }""")
    if self.weaknesses7.text() == 'Normal' or self.weaknesses7.text() == 'Normal +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: grey; color: black }""")
    if self.weaknesses7.text() == 'Ground' or self.weaknesses7.text() == 'Ground +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #a19027; color: black }""")
    if self.weaknesses7.text() == 'Ghost' or self.weaknesses7.text() == 'Ghost +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #784876; color: white }""")
    if self.weaknesses7.text() == 'Fire' or self.weaknesses7.text() == 'Fire +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: orange; color: white }""")
    if self.weaknesses7.text() == 'Fairy' or self.weaknesses7.text() == 'Fairy +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: pink; color: white }""")
    if self.weaknesses7.text() == 'Dragon' or self.weaknesses7.text() == 'Dragon +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: #c25b2f; color: white }""")
    if self.weaknesses7.text() == 'Bug' or self.weaknesses7.text() == 'Bug +4 Damage':
        self.weaknesses7.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
    if self.weaknesses7.text() == '':
        self.weaknesses7.setStyleSheet("")

    if self.pcolour.text() == 'Pink':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Pink }""")
    if self.pcolour.text() == 'White':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Black }""")
    if self.pcolour.text() == 'Gray':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Gray }""")
    if self.pcolour.text() == 'Purple':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Purple }""")
    if self.pcolour.text() == 'Brown':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Brown }""")
    if self.pcolour.text() == 'Black':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Black }""")
    if self.pcolour.text() == 'Green':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Green }""")
    if self.pcolour.text() == 'Yellow':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Yellow }""")
    if self.pcolour.text() == 'Blue':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Blue }""")
    if self.pcolour.text() == 'Red':
        self.pcolour.setStyleSheet("""QLineEdit { background-color: white; color: Red }""")
    if self.pcolour.text() == '':
        self.pcolour.setStyleSheet("")
    else:
        pass
