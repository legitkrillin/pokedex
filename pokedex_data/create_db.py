# Import sqlite3
import sqlite3

conn = sqlite3.connect('../pokedex.db')
c = conn.cursor()
c.execute("""CREATE TABLE IF NOT EXISTS pokemon (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        avatar text,
        number integer,
        name text,
        height text,
        weight integer,
        gender text,
        category text,
        abilities_1 text,
        abilities_2 text,
        type_1 text,
        type_2 text,
        weaknesses_1 text,
        weaknesses_2 text,
        weaknesses_3 text,
        weaknesses_4 text,
        weaknesses_5 text,
        weaknesses_6 text,
        weaknesses_7 text,
        kanto text,
        johto text,
        hoenn text,
        sinnoh text,
        unova text, 
        kalos text,
        alola text,
        galar text, 
        hp integer,
        attack integer,
        defense integer,
        special_attack integer,
        special_defense integer,
        speed integer,
        evolutions_1 text,
        evolutions_2 text,
        evolutions_3 text,
        evolutions_4 text,
        evolutions_5 text,
        evolutions_6 text,
        evolutions_7 text,
        evolutions_8 text,
        evolutions_9 text,
        mega text,
        gigantamax text,
        alolan text,
        galarian text, 
        bio text,
        generation text,
        pokedex_colour text,
        hidden_abilities text
        )""")


conn.commit()
conn.close()
