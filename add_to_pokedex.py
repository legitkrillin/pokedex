from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import *
# Import sqlite3
import sqlite3
from pokedex_data import pokemon_list_data
# Import csv
import csv

# Database
# Create a database or connect to one
conn = sqlite3.connect('pokedex.db')
# Create cursor
c = conn.cursor()
conn.close()


# exit app call
def exitapp():
    exit()


class Ui_Pokedex(object):
    def setupUi(self, Pokedex):
        Pokedex.setObjectName("Pokedex")
        Pokedex.setFixedSize(1095, 865)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("pokedex_data/index.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Pokedex.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(Pokedex)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(600, 460, 481, 361))
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(50)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(6, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(7, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(8, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(9, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(10, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(11, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(12, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(13, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(14, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(15, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(16, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(17, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(18, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(19, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(20, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(21, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(22, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(23, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(24, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(25, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(26, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(27, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(28, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(29, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(30, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(31, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(32, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(33, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(34, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(35, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(36, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(37, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(38, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(39, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(40, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(41, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(42, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(43, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(44, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(45, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(46, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(47, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(48, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(49, item)
        self.gridLayoutWidget_6 = QtWidgets.QWidget(self.centralwidget)
        self.gridLayoutWidget_6.setGeometry(QtCore.QRect(220, 10, 371, 811))
        self.gridLayoutWidget_6.setObjectName("gridLayoutWidget_6")
        self.grid2 = QtWidgets.QGridLayout(self.gridLayoutWidget_6)
        self.grid2.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
        self.grid2.setContentsMargins(0, 0, 0, 0)
        self.grid2.setObjectName("grid2")
        self.alolan = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.alolan.setObjectName("alolan")
        self.grid2.addWidget(self.alolan, 13, 2, 1, 1)
        self.lbpcolour = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbpcolour.setObjectName("lbpcolour")
        self.grid2.addWidget(self.lbpcolour, 14, 2, 1, 1)
        self.lbhoenn = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbhoenn.setObjectName("lbhoenn")
        self.grid2.addWidget(self.lbhoenn, 20, 2, 1, 1)
        self.lbgalarian = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbgalarian.setObjectName("lbgalarian")
        self.grid2.addWidget(self.lbgalarian, 14, 0, 1, 1)
        self.lbhp = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbhp.setObjectName("lbhp")
        self.grid2.addWidget(self.lbhp, 0, 0, 1, 1)
        self.lbspeed = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbspeed.setObjectName("lbspeed")
        self.grid2.addWidget(self.lbspeed, 6, 0, 1, 1)
        self.sinnoh = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.sinnoh.setObjectName("sinnoh")
        self.grid2.addWidget(self.sinnoh, 23, 0, 1, 1)
        self.lbgalar = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbgalar.setObjectName("lbgalar")
        self.grid2.addWidget(self.lbgalar, 24, 1, 1, 1)
        self.lbspattack = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbspattack.setObjectName("lbspattack")
        self.grid2.addWidget(self.lbspattack, 4, 0, 1, 1)
        self.galarian = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.galarian.setObjectName("galarian")
        self.grid2.addWidget(self.galarian, 15, 0, 1, 1)
        self.weaknesses1 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.weaknesses1.setObjectName("weaknesses1")
        self.grid2.addWidget(self.weaknesses1, 17, 0, 1, 1)
        self.lbspdefence = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbspdefence.setObjectName("lbspdefence")
        self.grid2.addWidget(self.lbspdefence, 5, 0, 1, 1)
        self.lbkanto = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbkanto.setObjectName("lbkanto")
        self.grid2.addWidget(self.lbkanto, 20, 0, 1, 1)
        self.johto = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.johto.setObjectName("johto")
        self.grid2.addWidget(self.johto, 21, 1, 1, 1)
        self.lbkalos = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbkalos.setObjectName("lbkalos")
        self.grid2.addWidget(self.lbkalos, 22, 2, 1, 1)
        self.giga = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.giga.setObjectName("giga")
        self.grid2.addWidget(self.giga, 13, 1, 1, 1)
        self.lbalola = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbalola.setObjectName("lbalola")
        self.grid2.addWidget(self.lbalola, 24, 0, 1, 1)
        self.galar = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.galar.setObjectName("galar")
        self.grid2.addWidget(self.galar, 25, 1, 1, 1)
        self.lbgeneration = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbgeneration.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.lbgeneration.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.lbgeneration.setFrameShadow(QtWidgets.QFrame.Plain)
        self.lbgeneration.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.lbgeneration.setObjectName("lbgeneration")
        self.grid2.addWidget(self.lbgeneration, 14, 1, 1, 1)
        self.weaknesses4 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.weaknesses4.setObjectName("weaknesses4")
        self.grid2.addWidget(self.weaknesses4, 18, 0, 1, 1)
        self.alola = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.alola.setObjectName("alola")
        self.grid2.addWidget(self.alola, 25, 0, 1, 1)
        self.lbgiga = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbgiga.setObjectName("lbgiga")
        self.grid2.addWidget(self.lbgiga, 12, 1, 1, 1)
        self.weaknesses2 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.weaknesses2.setObjectName("weaknesses2")
        self.grid2.addWidget(self.weaknesses2, 17, 1, 1, 1)
        self.lbjohto = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbjohto.setObjectName("lbjohto")
        self.grid2.addWidget(self.lbjohto, 20, 1, 1, 1)
        self.mega = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.mega.setObjectName("mega")
        self.grid2.addWidget(self.mega, 13, 0, 1, 1)
        self.evolutions4 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions4.setObjectName("evolutions4")
        self.grid2.addWidget(self.evolutions4, 10, 0, 1, 1)
        self.lbsinnoh = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbsinnoh.setObjectName("lbsinnoh")
        self.grid2.addWidget(self.lbsinnoh, 22, 0, 1, 1)
        self.weaknesses5 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.weaknesses5.setObjectName("weaknesses5")
        self.grid2.addWidget(self.weaknesses5, 18, 1, 1, 1)
        self.hoenn = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.hoenn.setObjectName("hoenn")
        self.grid2.addWidget(self.hoenn, 21, 2, 1, 1)
        self.lbattack = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbattack.setObjectName("lbattack")
        self.grid2.addWidget(self.lbattack, 2, 0, 1, 1)
        self.evolutions5 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions5.setObjectName("evolutions5")
        self.grid2.addWidget(self.evolutions5, 10, 1, 1, 1)
        self.lbdefense = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbdefense.setObjectName("lbdefense")
        self.grid2.addWidget(self.lbdefense, 3, 0, 1, 1)
        self.lbmega = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbmega.setObjectName("lbmega")
        self.grid2.addWidget(self.lbmega, 12, 0, 1, 1)
        self.evolutions1 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions1.setObjectName("evolutions1")
        self.grid2.addWidget(self.evolutions1, 9, 0, 1, 1)
        self.evolutions7 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions7.setObjectName("evolutions7")
        self.grid2.addWidget(self.evolutions7, 11, 0, 1, 1)
        self.kanto = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.kanto.setObjectName("kanto")
        self.grid2.addWidget(self.kanto, 21, 0, 1, 1)
        self.kalos = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.kalos.setObjectName("kalos")
        self.grid2.addWidget(self.kalos, 23, 2, 1, 1)
        self.evolutions9 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions9.setObjectName("evolutions9")
        self.grid2.addWidget(self.evolutions9, 11, 2, 1, 1)
        self.weaknesses7 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.weaknesses7.setObjectName("weaknesses7")
        self.grid2.addWidget(self.weaknesses7, 19, 0, 1, 1)
        self.pcolour = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.pcolour.setObjectName("pcolour")
        self.grid2.addWidget(self.pcolour, 15, 2, 1, 1)
        self.evolutions3 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions3.setObjectName("evolutions3")
        self.grid2.addWidget(self.evolutions3, 9, 2, 1, 1)
        self.generation = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.generation.setObjectName("generation")
        self.grid2.addWidget(self.generation, 15, 1, 1, 1)
        self.evolutions6 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions6.setObjectName("evolutions6")
        self.grid2.addWidget(self.evolutions6, 10, 2, 1, 1)
        self.lbunova = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbunova.setObjectName("lbunova")
        self.grid2.addWidget(self.lbunova, 22, 1, 1, 1)
        self.lbweaknesses = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbweaknesses.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.lbweaknesses.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.lbweaknesses.setObjectName("lbweaknesses")
        self.grid2.addWidget(self.lbweaknesses, 16, 0, 1, 3)
        self.weaknesses6 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.weaknesses6.setObjectName("weaknesses6")
        self.grid2.addWidget(self.weaknesses6, 18, 2, 1, 1)
        self.lbevo = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbevo.setObjectName("lbevo")
        self.grid2.addWidget(self.lbevo, 7, 0, 1, 3)
        self.unova = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.unova.setObjectName("unova")
        self.grid2.addWidget(self.unova, 23, 1, 1, 1)
        self.evolutions8 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions8.setObjectName("evolutions8")
        self.grid2.addWidget(self.evolutions8, 11, 1, 1, 1)
        self.lbalolan = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbalolan.setObjectName("lbalolan")
        self.grid2.addWidget(self.lbalolan, 12, 2, 1, 1)
        self.weaknesses3 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.weaknesses3.setObjectName("weaknesses3")
        self.grid2.addWidget(self.weaknesses3, 17, 2, 1, 1)
        self.evolutions2 = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.evolutions2.setObjectName("evolutions2")
        self.grid2.addWidget(self.evolutions2, 9, 1, 1, 1)
        self.hp = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.hp.setObjectName("hp")
        self.grid2.addWidget(self.hp, 0, 1, 1, 2)
        self.attack = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.attack.setObjectName("attack")
        self.grid2.addWidget(self.attack, 2, 1, 1, 2)
        self.defence = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.defence.setObjectName("defence")
        self.grid2.addWidget(self.defence, 3, 1, 1, 2)
        self.spattack = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.spattack.setObjectName("spattack")
        self.grid2.addWidget(self.spattack, 4, 1, 1, 2)
        self.spdefence = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.spdefence.setObjectName("spdefence")
        self.grid2.addWidget(self.spdefence, 5, 1, 1, 2)
        self.speed = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.speed.setObjectName("speed")
        self.grid2.addWidget(self.speed, 6, 1, 1, 2)
        self.lbavatar = QtWidgets.QLabel(self.gridLayoutWidget_6)
        self.lbavatar.setObjectName("lbavatar")
        self.grid2.addWidget(self.lbavatar, 24, 2, 1, 1)
        self.add_avatar = QtWidgets.QLineEdit(self.gridLayoutWidget_6)
        self.add_avatar.setObjectName("avatar")
        self.grid2.addWidget(self.add_avatar, 25, 2, 1, 1)
        self.gridLayoutWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.gridLayoutWidget_2.setGeometry(QtCore.QRect(10, 10, 201, 630))
        self.gridLayoutWidget_2.setObjectName("gridLayoutWidget_2")
        self.grid1 = QtWidgets.QGridLayout(self.gridLayoutWidget_2)
        self.grid1.setContentsMargins(0, 0, 0, 0)
        self.grid1.setObjectName("grid1")
        self.lbcategory = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbcategory.setObjectName("lbcategory")
        self.grid1.addWidget(self.lbcategory, 10, 0, 1, 1)
        self.lbweight = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbweight.setObjectName("lbweight")
        self.grid1.addWidget(self.lbweight, 6, 0, 1, 1)
        self.lbdex = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbdex.setObjectName("lbdex")
        self.grid1.addWidget(self.lbdex, 2, 0, 1, 1)
        self.lbname = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbname.setObjectName("lbname")
        self.grid1.addWidget(self.lbname, 0, 0, 1, 1)
        self.weight = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.weight.setObjectName("weight")
        self.grid1.addWidget(self.weight, 7, 0, 1, 1)
        self.habilities = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.habilities.setObjectName("habilities")
        self.grid1.addWidget(self.habilities, 20, 0, 1, 1)
        self.lbtype = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbtype.setObjectName("lbtype")
        self.grid1.addWidget(self.lbtype, 12, 0, 1, 1)
        self.lbabilities = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbabilities.setObjectName("lbabilities")
        self.grid1.addWidget(self.lbabilities, 16, 0, 1, 1)
        self.lbheight = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbheight.setObjectName("lbheight")
        self.grid1.addWidget(self.lbheight, 4, 0, 1, 1)
        self.dex = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.dex.setObjectName("dex")
        self.grid1.addWidget(self.dex, 3, 0, 1, 1)
        self.type2 = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.type2.setObjectName("type2")
        self.grid1.addWidget(self.type2, 15, 0, 1, 1)
        self.type1 = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.type1.setObjectName("type1")
        self.grid1.addWidget(self.type1, 14, 0, 1, 1)
        self.gender = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.gender.setObjectName("gender")
        self.grid1.addWidget(self.gender, 9, 0, 1, 1)
        self.lbhabilities = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbhabilities.setObjectName("lbhabilities")
        self.grid1.addWidget(self.lbhabilities, 19, 0, 1, 1)
        self.name = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.name.setObjectName("name")
        self.grid1.addWidget(self.name, 1, 0, 1, 1)
        self.abilities2 = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.abilities2.setObjectName("abilities2")
        self.grid1.addWidget(self.abilities2, 18, 0, 1, 1)
        self.height = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.height.setReadOnly(False)
        self.height.setObjectName("height")
        self.grid1.addWidget(self.height, 5, 0, 1, 1)
        self.abilities1 = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.abilities1.setObjectName("abilities1")
        self.grid1.addWidget(self.abilities1, 17, 0, 1, 1)
        self.lbgender = QtWidgets.QLabel(self.gridLayoutWidget_2)
        self.lbgender.setObjectName("lbgender")
        self.grid1.addWidget(self.lbgender, 8, 0, 1, 1)
        self.category = QtWidgets.QLineEdit(self.gridLayoutWidget_2)
        self.category.setObjectName("category")
        self.grid1.addWidget(self.category, 11, 0, 1, 1)
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(600, 370, 481, 81))
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.add = QtWidgets.QPushButton(self.centralwidget)
        self.add.setGeometry(QtCore.QRect(10, 650, 201, 81))
        self.add.setObjectName("add")
        self.update = QtWidgets.QPushButton(self.centralwidget)
        self.update.setGeometry(QtCore.QRect(10, 740, 201, 81))
        self.update.setObjectName("update")
        self.graphicsView = QtWidgets.QGraphicsView(self.centralwidget)
        self.graphicsView.setGeometry(QtCore.QRect(600, 10, 481, 351))
        self.graphicsView.setObjectName("graphicsView")
        Pokedex.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Pokedex)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1095, 30))
        self.menubar.setObjectName("menubar")
        self.menuExit = QtWidgets.QMenu(self.menubar)
        self.menuExit.setObjectName("menuExit")
        self.menuDatabase = QtWidgets.QMenu(self.menubar)
        self.menuDatabase.setObjectName("menuDatabase")
        Pokedex.setMenuBar(self.menubar)
        self.actionExit = QtWidgets.QAction(Pokedex)
        self.actionExit.setObjectName("actionExit")
        self.actionReload_Database = QtWidgets.QAction(Pokedex)
        self.actionReload_Database.setObjectName("actionReload_Database")
        self.actionExport_to_CSV = QtWidgets.QAction(Pokedex)
        self.actionExport_to_CSV.setObjectName("actionExport_to_CSV")
        self.actionAbout = QtWidgets.QAction(Pokedex)
        self.actionAbout.setObjectName("actionAbout")
        self.menuExit.addAction(self.actionExit)
        self.menuExit.addAction(self.actionAbout)
        self.menuDatabase.addAction(self.actionReload_Database)
        self.menuDatabase.addAction(self.actionExport_to_CSV)
        self.menubar.addAction(self.menuExit.menuAction())
        self.menubar.addAction(self.menuDatabase.menuAction())

        self.retranslateUi(Pokedex)
        QtCore.QMetaObject.connectSlotsByName(Pokedex)
        Pokedex.setTabOrder(self.name, self.dex)
        Pokedex.setTabOrder(self.dex, self.height)
        Pokedex.setTabOrder(self.height, self.weight)
        Pokedex.setTabOrder(self.weight, self.gender)
        Pokedex.setTabOrder(self.gender, self.category)
        Pokedex.setTabOrder(self.category, self.type1)
        Pokedex.setTabOrder(self.type1, self.type2)
        Pokedex.setTabOrder(self.type2, self.abilities1)
        Pokedex.setTabOrder(self.abilities1, self.abilities2)
        Pokedex.setTabOrder(self.abilities2, self.habilities)
        Pokedex.setTabOrder(self.habilities, self.hp)
        Pokedex.setTabOrder(self.hp, self.attack)
        Pokedex.setTabOrder(self.attack, self.defence)
        Pokedex.setTabOrder(self.defence, self.spattack)
        Pokedex.setTabOrder(self.spattack, self.spdefence)
        Pokedex.setTabOrder(self.spdefence, self.speed)
        Pokedex.setTabOrder(self.speed, self.evolutions1)
        Pokedex.setTabOrder(self.evolutions1, self.evolutions2)
        Pokedex.setTabOrder(self.evolutions2, self.evolutions3)
        Pokedex.setTabOrder(self.evolutions3, self.evolutions4)
        Pokedex.setTabOrder(self.evolutions4, self.evolutions5)
        Pokedex.setTabOrder(self.evolutions5, self.evolutions6)
        Pokedex.setTabOrder(self.evolutions6, self.evolutions7)
        Pokedex.setTabOrder(self.evolutions7, self.evolutions8)
        Pokedex.setTabOrder(self.evolutions8, self.evolutions9)
        Pokedex.setTabOrder(self.evolutions9, self.mega)
        Pokedex.setTabOrder(self.mega, self.giga)
        Pokedex.setTabOrder(self.giga, self.alolan)
        Pokedex.setTabOrder(self.alolan, self.galarian)
        Pokedex.setTabOrder(self.galarian, self.generation)
        Pokedex.setTabOrder(self.generation, self.pcolour)
        Pokedex.setTabOrder(self.pcolour, self.weaknesses1)
        Pokedex.setTabOrder(self.weaknesses1, self.weaknesses2)
        Pokedex.setTabOrder(self.weaknesses2, self.weaknesses3)
        Pokedex.setTabOrder(self.weaknesses3, self.weaknesses4)
        Pokedex.setTabOrder(self.weaknesses4, self.weaknesses5)
        Pokedex.setTabOrder(self.weaknesses5, self.weaknesses6)
        Pokedex.setTabOrder(self.weaknesses6, self.weaknesses7)
        Pokedex.setTabOrder(self.weaknesses7, self.kanto)
        Pokedex.setTabOrder(self.kanto, self.johto)
        Pokedex.setTabOrder(self.johto, self.hoenn)
        Pokedex.setTabOrder(self.hoenn, self.sinnoh)
        Pokedex.setTabOrder(self.sinnoh, self.unova)
        Pokedex.setTabOrder(self.unova, self.kalos)
        Pokedex.setTabOrder(self.kalos, self.alola)
        Pokedex.setTabOrder(self.alola, self.galar)
        Pokedex.setTabOrder(self.galar, self.add)
        Pokedex.setTabOrder(self.add, self.update)
        Pokedex.setTabOrder(self.update, self.graphicsView)
        Pokedex.setTabOrder(self.graphicsView, self.plainTextEdit)
        Pokedex.setTabOrder(self.plainTextEdit, self.tableWidget)

    def retranslateUi(self, Pokedex):
        _translate = QtCore.QCoreApplication.translate
        Pokedex.setWindowTitle(_translate("Pokedex", "Pokémon - Pokédex"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Pokedex", "ID"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("Pokedex", "Avatar"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("Pokedex", "Dex #"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("Pokedex", "Name"))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("Pokedex", "Height"))
        item = self.tableWidget.horizontalHeaderItem(5)
        item.setText(_translate("Pokedex", "Weight"))
        item = self.tableWidget.horizontalHeaderItem(6)
        item.setText(_translate("Pokedex", "Gender"))
        item = self.tableWidget.horizontalHeaderItem(7)
        item.setText(_translate("Pokedex", "Category"))
        item = self.tableWidget.horizontalHeaderItem(8)
        item.setText(_translate("Pokedex", "Abilities 1"))
        item = self.tableWidget.horizontalHeaderItem(9)
        item.setText(_translate("Pokedex", "Abilities 2"))
        item = self.tableWidget.horizontalHeaderItem(10)
        item.setText(_translate("Pokedex", "Type 1"))
        item = self.tableWidget.horizontalHeaderItem(11)
        item.setText(_translate("Pokedex", "Type 2"))
        item = self.tableWidget.horizontalHeaderItem(12)
        item.setText(_translate("Pokedex", "Weaknesses 1"))
        item = self.tableWidget.horizontalHeaderItem(13)
        item.setText(_translate("Pokedex", "Weaknesses 2"))
        item = self.tableWidget.horizontalHeaderItem(14)
        item.setText(_translate("Pokedex", "Weaknesses 3"))
        item = self.tableWidget.horizontalHeaderItem(15)
        item.setText(_translate("Pokedex", "Weaknesses 4"))
        item = self.tableWidget.horizontalHeaderItem(16)
        item.setText(_translate("Pokedex", "Weaknesses 5"))
        item = self.tableWidget.horizontalHeaderItem(17)
        item.setText(_translate("Pokedex", "Weaknesses 6"))
        item = self.tableWidget.horizontalHeaderItem(18)
        item.setText(_translate("Pokedex", "Weaknesses 7"))
        item = self.tableWidget.horizontalHeaderItem(19)
        item.setText(_translate("Pokedex", "Kanto"))
        item = self.tableWidget.horizontalHeaderItem(20)
        item.setText(_translate("Pokedex", "Johto"))
        item = self.tableWidget.horizontalHeaderItem(21)
        item.setText(_translate("Pokedex", "Hoenn"))
        item = self.tableWidget.horizontalHeaderItem(22)
        item.setText(_translate("Pokedex", "Sinnoh"))
        item = self.tableWidget.horizontalHeaderItem(23)
        item.setText(_translate("Pokedex", "Unova"))
        item = self.tableWidget.horizontalHeaderItem(24)
        item.setText(_translate("Pokedex", "Kalos"))
        item = self.tableWidget.horizontalHeaderItem(25)
        item.setText(_translate("Pokedex", "Alola"))
        item = self.tableWidget.horizontalHeaderItem(26)
        item.setText(_translate("Pokedex", "Galar"))
        item = self.tableWidget.horizontalHeaderItem(27)
        item.setText(_translate("Pokedex", "HP"))
        item = self.tableWidget.horizontalHeaderItem(28)
        item.setText(_translate("Pokedex", "Attack"))
        item = self.tableWidget.horizontalHeaderItem(29)
        item.setText(_translate("Pokedex", "Defense"))
        item = self.tableWidget.horizontalHeaderItem(30)
        item.setText(_translate("Pokedex", "Special Attack"))
        item = self.tableWidget.horizontalHeaderItem(31)
        item.setText(_translate("Pokedex", "Special Defense"))
        item = self.tableWidget.horizontalHeaderItem(32)
        item.setText(_translate("Pokedex", "Speed"))
        item = self.tableWidget.horizontalHeaderItem(33)
        item.setText(_translate("Pokedex", "Evolutions 1"))
        item = self.tableWidget.horizontalHeaderItem(34)
        item.setText(_translate("Pokedex", "Evolutions 2"))
        item = self.tableWidget.horizontalHeaderItem(35)
        item.setText(_translate("Pokedex", "Evolutions 3"))
        item = self.tableWidget.horizontalHeaderItem(36)
        item.setText(_translate("Pokedex", "Evolutions 4"))
        item = self.tableWidget.horizontalHeaderItem(37)
        item.setText(_translate("Pokedex", "Evolutions 5"))
        item = self.tableWidget.horizontalHeaderItem(38)
        item.setText(_translate("Pokedex", "Evolutions 6"))
        item = self.tableWidget.horizontalHeaderItem(39)
        item.setText(_translate("Pokedex", "Evolutions 7"))
        item = self.tableWidget.horizontalHeaderItem(40)
        item.setText(_translate("Pokedex", "Evolutions 8"))
        item = self.tableWidget.horizontalHeaderItem(41)
        item.setText(_translate("Pokedex", "Evolutions 9"))
        item = self.tableWidget.horizontalHeaderItem(42)
        item.setText(_translate("Pokedex", "Mega"))
        item = self.tableWidget.horizontalHeaderItem(43)
        item.setText(_translate("Pokedex", "Gigantamax"))
        item = self.tableWidget.horizontalHeaderItem(44)
        item.setText(_translate("Pokedex", "Alolan"))
        item = self.tableWidget.horizontalHeaderItem(45)
        item.setText(_translate("Pokedex", "Galarian"))
        item = self.tableWidget.horizontalHeaderItem(46)
        item.setText(_translate("Pokedex", "Bio"))
        item = self.tableWidget.horizontalHeaderItem(47)
        item.setText(_translate("Pokedex", "Generation"))
        item = self.tableWidget.horizontalHeaderItem(48)
        item.setText(_translate("Pokedex", "Pokedex Colour"))
        item = self.tableWidget.horizontalHeaderItem(49)
        item.setText(_translate("Pokedex", "Hidden Abilities"))
        self.lbpcolour.setText(_translate("Pokedex", "Pokédex colour"))
        self.lbhoenn.setText(_translate("Pokedex", "Hoenn"))
        self.lbgalarian.setText(_translate("Pokedex", "Galarian:"))
        self.lbhp.setText(_translate("Pokedex", "HP"))
        self.lbspeed.setText(_translate("Pokedex", "Speed"))
        self.lbgalar.setText(_translate("Pokedex", "Galar"))
        self.lbspattack.setText(_translate("Pokedex", "Sp. Attack"))
        self.lbspdefence.setText(_translate("Pokedex", "Sp. Defense"))
        self.lbkanto.setText(_translate("Pokedex", "Kanto"))
        self.lbkalos.setText(_translate("Pokedex", "Kalos"))
        self.lbalola.setText(_translate("Pokedex", "Alola"))
        self.lbgeneration.setText(_translate("Pokedex", "Generation:  "))
        self.lbgiga.setText(_translate("Pokedex", "Gigantamax:"))
        self.lbjohto.setText(_translate("Pokedex", "Johto"))
        self.lbsinnoh.setText(_translate("Pokedex", "Sinnoh"))
        self.lbattack.setText(_translate("Pokedex", "Attack"))
        self.lbdefense.setText(_translate("Pokedex", "Defense"))
        self.lbmega.setText(_translate("Pokedex", "Mega:"))
        self.lbunova.setText(_translate("Pokedex", "Unova"))
        self.lbweaknesses.setText(_translate("Pokedex", "Weaknesses:"))
        self.lbevo.setText(_translate("Pokedex", "Evolutions:"))
        self.lbalolan.setText(_translate("Pokedex", "Alolan:"))
        self.lbavatar.setText(_translate("Pokedex", "Avatar"))
        self.lbcategory.setText(_translate("Pokedex", "Category:"))
        self.lbweight.setText(_translate("Pokedex", "Weight:"))
        self.lbdex.setText(_translate("Pokedex", "Dex #"))
        self.lbname.setText(_translate("Pokedex", "Name:"))
        self.lbtype.setText(_translate("Pokedex", "Type:"))
        self.lbabilities.setText(_translate("Pokedex", "Abilities:"))
        self.lbheight.setText(_translate("Pokedex", "Height:"))
        self.lbhabilities.setText(_translate("Pokedex", "Hidden Abilities:"))
        self.lbgender.setText(_translate("Pokedex", "Gender:"))
        self.add.setText(_translate("Pokedex", "ADD TO POKEDEX"))
        self.update.setText(_translate("Pokedex", "UPDATE"))
        self.menuExit.setTitle(_translate("Pokedex", "File"))
        self.menuDatabase.setTitle(_translate("Pokedex", "Database"))
        self.actionExit.setText(_translate("Pokedex", "Exit"))
        self.actionReload_Database.setText(_translate("Pokedex", "Reload Database"))
        self.actionExport_to_CSV.setText(_translate("Pokedex", "Export DB to CSV"))
        self.actionAbout.setText(_translate("Pokedex", "About"))

        # hide number column on the left hand side of table widget
        self.tableWidget.verticalHeader().setVisible(False)
        # hide grid in table widget
        self.tableWidget.setShowGrid(False)
        # select whole row
        self.tableWidget.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
        self.tableWidget.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.tableWidget.setAlternatingRowColors(True)
        header = self.tableWidget.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        # enable sorting on tabs in grid view
        self.tableWidget.setSortingEnabled(True)
        # menu bar connect, exit app call
        self.actionExit.triggered.connect(exitapp)
        self.actionReload_Database.triggered.connect(self.query)
        self.actionAbout.triggered.connect(self.about)
        self.actionExport_to_CSV.triggered.connect(self.dbtocsv)
        self.tableWidget.doubleClicked.connect(self.doubleclick)
        Ui_Pokedex.query(self)
        self.tableWidget.hideColumn(0)
        self.tableWidget.hideColumn(1)
        self.tableWidget.hideColumn(36)
        self.tableWidget.hideColumn(37)
        self.tableWidget.hideColumn(38)
        self.tableWidget.hideColumn(39)
        self.tableWidget.hideColumn(40)
        self.tableWidget.hideColumn(41)
        self.tableWidget.hideColumn(46)
        completer = QCompleter(pokemon_list_data.type_weakness)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.weaknesses1.setCompleter(completer)
        self.weaknesses2.setCompleter(completer)
        self.weaknesses3.setCompleter(completer)
        self.weaknesses4.setCompleter(completer)
        self.weaknesses5.setCompleter(completer)
        self.weaknesses6.setCompleter(completer)
        self.weaknesses7.setCompleter(completer)
        self.type1.setCompleter(completer)
        self.type2.setCompleter(completer)

        gender = QCompleter(pokemon_list_data.gender)
        gender.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.gender.setCompleter(gender)

        ability = QCompleter(pokemon_list_data.ability_list)
        ability.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.abilities1.setCompleter(ability)
        self.abilities2.setCompleter(ability)

        generation = QCompleter(pokemon_list_data.gen)
        generation.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.generation.setCompleter(generation)

        colour = QCompleter(pokemon_list_data.pokedex_colour)
        colour.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.pcolour.setCompleter(colour)

        self.gender.setText("Male and Female")
        self.add.clicked.connect(self.submit)
        self.update.clicked.connect(self.update_db)

    # Create submit function for DB
    def submit(self):
        if self.add_avatar.text() == "":
            # error message for empty field
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Information)
            msgBox.setText("Please enter valid Pokémon Avatar!")
            msgBox.setWindowTitle("Missing Info")
            msgBox.setStandardButtons(QMessageBox.Ok)
            msgBox.exec_()
            self.add_avatar.setFocus()
            return

        str = self.add_avatar.strip()
        res_str = str.replace('file:///home/james/PycharmProjects/pythonProject/Pokedexv3', '.')
        conn = sqlite3.connect('pokedex.db')
        # Create cursor
        c = conn.cursor()
        c.execute(
            "INSERT INTO pokemon VALUES (:id, :avatar, :number, :name, :height, :weight, :gender, "
            ":category, :abilities_1, :abilities_2, :type_1, :type_2, :weaknesses_1, :weaknesses_2, "
            ":weaknesses_3, :weaknesses_4, :weaknesses_5, :weaknesses_6, :weaknesses_7,  "
            ":kanto, :johto, :hoenn, :sinnoh, :unova, :kalos, :alola, :galar, "
            ":hp, :attack, :defense, :special_attack, :special_defense, :speed, "
            ":evolutions_1, :evolutions_2, :evolutions_3, :evolutions_4, "
            ":evolutions_5, :evolutions_6, :evolutions_7, :evolutions_8, :evolutions_9,:mega, "
            ":gigantamax, :alolan, :galarian,"
            ":bio, :generation, :pokedex_colour, :hidden_abilities)",
            {
                'id': None,
                'avatar': res_str,
                'number': self.dex.text(),
                'name': self.name.text(),
                'height': self.height.text(),
                'weight': self.weight.text(),
                'gender': self.gender.text(),
                'category': self.category.text(),
                'abilities_1': self.abilities1.text(),
                'abilities_2': self.abilities2.text(),
                'type_1': self.type1.text(),
                'type_2': self.type2.text(),
                'weaknesses_1': self.weaknesses1.text(),
                'weaknesses_2': self.weaknesses2.text(),
                'weaknesses_3': self.weaknesses3.text(),
                'weaknesses_4': self.weaknesses4.text(),
                'weaknesses_5': self.weaknesses5.text(),
                'weaknesses_6': self.weaknesses6.text(),
                'weaknesses_7': self.weaknesses7.text(),
                'kanto': self.kanto.text(),
                'johto': self.johto.text(),
                'hoenn': self.hoenn.text(),
                'sinnoh': self.sinnoh.text(),
                'unova': self.unova.text(),
                'kalos': self.kalos.text(),
                'alola': self.alola.text(),
                'galar': self.galar.text(),
                'hp': self.hp.text(),
                'attack': self.attack.text(),
                'defense': self.defence.text(),
                'special_attack': self.spattack.text(),
                'special_defense': self.spdefence.text(),
                'speed': self.speed.text(),
                'evolutions_1': self.evolutions1.text(),
                'evolutions_2': self.evolutions2.text(),
                'evolutions_3': self.evolutions3.text(),
                'evolutions_4': self.evolutions4.text(),
                'evolutions_5': self.evolutions5.text(),
                'evolutions_6': self.evolutions6.text(),
                'evolutions_7': self.evolutions7.text(),
                'evolutions_8': self.evolutions8.text(),
                'evolutions_9': self.evolutions9.text(),
                'mega': self.mega.text(),
                'gigantamax': self.giga.text(),
                'alolan': self.alolan.text(),
                'galarian': self.galarian.text(),
                'bio': self.plainTextEdit.toPlainText(),
                'generation': self.generation.text(),
                'pokedex_colour': self.pcolour.text(),
                'hidden_abilities': self.habilities.text()
            })
        # Commit changes to DB
        conn.commit()
        # Close connection to DB
        conn.close()
        Ui_Pokedex.clear_all(self)

    def query(self):
        global item
        Ui_Pokedex.clear_all(self)
        self.tableWidget.setRowCount(0)
        self.tableWidget.horizontalHeader().setSortIndicator(-1, 0)
        self.tableWidget.setSortingEnabled(True)
        # Create a database or connect to one
        conn = sqlite3.connect('pokedex.db')
        # Create cursor
        c = conn.cursor()
        # Query the DB
        c.execute("SELECT * FROM pokemon")
        records = c.fetchall()
        for row_count, hardware in enumerate(records):
            self.tableWidget.insertRow(row_count)
            for column_number, data in enumerate(hardware):
                cell = QtWidgets.QTableWidgetItem(str(data))
                self.tableWidget.setItem(row_count, column_number, cell)

        pokemon = []
        for name in records:
            pokemon.append(name[3])
            completer = QCompleter(pokemon)
            completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
            self.evolutions1.setCompleter(completer)
            self.evolutions2.setCompleter(completer)
            self.evolutions3.setCompleter(completer)
            self.evolutions4.setCompleter(completer)
            self.evolutions5.setCompleter(completer)
            self.evolutions6.setCompleter(completer)
            self.evolutions7.setCompleter(completer)
            self.evolutions8.setCompleter(completer)
            self.evolutions9.setCompleter(completer)

        # Commit changes to DB
        conn.commit()
        # Close connection to DB
        conn.close()

    def getimagelable(self, temp_pix):
        imageLabel = QtWidgets.QLabel(self.centralwidget)
        imageLabel.setText("")
        imageLabel.setScaledContents(True)
        pixmap = QtGui.QPixmap(temp_pix)
        imageLabel.setPixmap(pixmap)
        return imageLabel

    def clear_all(self):
        # Clear the text boxes
        self.dex.clear()
        self.name.clear()
        self.height.clear()
        self.weight.clear()
        self.category.clear()
        self.abilities1.clear()
        self.abilities2.clear()
        self.type1.clear()
        self.type2.clear()
        self.weaknesses1.clear()
        self.weaknesses2.clear()
        self.weaknesses3.clear()
        self.weaknesses4.clear()
        self.weaknesses5.clear()
        self.weaknesses6.clear()
        self.weaknesses7.clear()
        self.kanto.clear()
        self.johto.clear()
        self.hoenn.clear()
        self.sinnoh.clear()
        self.unova.clear()
        self.kalos.clear()
        self.alola.clear()
        self.galar.clear()
        self.hp.clear()
        self.attack.clear()
        self.defence.clear()
        self.spattack.clear()
        self.spdefence.clear()
        self.speed.clear()
        self.evolutions1.clear()
        self.evolutions2.clear()
        self.evolutions3.clear()
        self.evolutions4.clear()
        self.evolutions5.clear()
        self.evolutions6.clear()
        self.evolutions7.clear()
        self.evolutions8.clear()
        self.evolutions9.clear()
        self.mega.clear()
        self.giga.clear()
        self.alolan.clear()
        self.galarian.clear()
        self.plainTextEdit.clear()
        self.generation.clear()
        self.pcolour.clear()
        self.habilities.clear()
        self.plainTextEdit.clear()

# doubleclick function to get id from column
    def doubleclick(self):
        self.add_avatar.clear()
        self.plainTextEdit.clear()
        self.avatar = QPixmap()
        index = (self.tableWidget.selectionModel().currentIndex())
        value = index.siblingAtColumn(0).data()
        # Create a database or connect to one
        conn = sqlite3.connect('pokedex.db')
        # Create cursor
        c = conn.cursor()
        c.execute("SELECT * FROM pokemon WHERE id = " + value)

        records = c.fetchall()

        for record in records:
            self.dex.setText(str(record[2]))
            self.name.setText(str(record[3]))
            self.height.setText(str(record[4]))
            self.weight.setText(str(record[5]))
            self.gender.setText(str(record[6]))
            self.category.setText(str(record[7]))
            self.abilities1.setText(str(record[8]))
            self.abilities2.setText(str(record[9]))
            self.type1.setText(str(record[10]))
            self.type2.setText(str(record[11]))
            self.weaknesses1.setText(str(record[12]))
            self.weaknesses2.setText(str(record[13]))
            self.weaknesses3.setText(str(record[14]))
            self.weaknesses4.setText(str(record[15]))
            self.weaknesses5.setText(str(record[16]))
            self.weaknesses6.setText(str(record[17]))
            self.weaknesses7.setText(str(record[18]))
            self.kanto.setText(str(record[19]))
            self.johto.setText(str(record[20]))
            self.hoenn.setText(str(record[21]))
            self.sinnoh.setText(str(record[22]))
            self.unova.setText(str(record[23]))
            self.kalos.setText(str(record[24]))
            self.alola.setText(str(record[25]))
            self.galar.setText(str(record[26]))
            self.hp.setText(str(record[27]))
            self.attack.setText(str(record[28]))
            self.defence.setText(str(record[29]))
            self.spattack.setText(str(record[30]))
            self.spdefence.setText(str(record[31]))
            self.speed.setText(str(record[32]))
            self.evolutions1.setText(str(record[33]))
            self.evolutions2.setText(str(record[34]))
            self.evolutions3.setText(str(record[35]))
            self.evolutions4.setText(str(record[36]))
            self.evolutions5.setText(str(record[37]))
            self.evolutions6.setText(str(record[38]))
            self.evolutions7.setText(str(record[39]))
            self.evolutions8.setText(str(record[40]))
            self.evolutions9.setText(str(record[41]))
            self.mega.setText(str(record[42]))
            self.giga.setText(str(record[43]))
            self.alolan.setText(str(record[44]))
            self.galarian.setText(str(record[45]))
            self.plainTextEdit.insertPlainText(str(record[46]))
            self.generation.setText(str(record[47]))
            self.pcolour.setText(str(record[48]))
            self.habilities.setText(str(record[49]))
            self.avatar.load(record[1])
            self.scene = QGraphicsScene()
            self.scene.addPixmap(QPixmap(self.avatar.scaled(340, 340)))
            self.graphicsView.setScene(self.scene)

            global temp_id
            temp_id = (str(record[0]))
            global temp_pix
            temp_pix = (str(record[1]))

    def update_db(self):
        if self.name.text() == "":
            # error message for empty field
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Information)
            msgBox.setText("Please select a valid Pokémon entry!")
            msgBox.setWindowTitle("Missing Info")
            msgBox.setStandardButtons(QMessageBox.Ok)
            msgBox.exec_()
            return
        self.tableWidget.horizontalHeader().setSortIndicator(-1, 0)
        self.tableWidget.setSortingEnabled(True)
        # Create a database or connect to one
        conn = sqlite3.connect('pokedex.db')
        # Create cursor
        c = conn.cursor()

        c.execute(
            "UPDATE pokemon SET number = :number, name = :name, height = :height, "
            "weight = :weight, gender = :gender, category = :category, "
            "abilities_1 = :abilities_1, abilities_2 = :abilities_2, type_1 = :type_1,  "
            "type_2 = :type_2, weaknesses_1 = :weaknesses_1, weaknesses_2 = :weaknesses_2, "
            "weaknesses_3 = :weaknesses_3, weaknesses_4 = :weaknesses_4, weaknesses_5 = :weaknesses_5, "
            "weaknesses_6 = :weaknesses_6, weaknesses_7 = :weaknesses_7, kanto = :kanto, johto = :johto, "
            "hoenn = :hoenn, sinnoh = :sinnoh, unova = :unova, kalos = :kalos, alola = :alola, "
            "galar = :galar,hp = :hp, "
            "attack = :attack, defense = :defense, special_attack = :special_attack, "
            "special_defense = :special_defense, speed = :speed, evolutions_1 = :evolutions_1, "
            "evolutions_2 = :evolutions_2, evolutions_3 = :evolutions_3, evolutions_4 = :evolutions_4, evolutions_5 = "
            ":evolutions_5, evolutions_6 = :evolutions_6, evolutions_7 = :evolutions_7, evolutions_8 = :evolutions_8, "
            "evolutions_9 = :evolutions_9, mega = :mega, "
            "gigantamax = :gigantamax, alolan = :alolan, galarian = :galarian, "
            "bio = :bio, generation = :generation, "
            "pokedex_colour = :pokedex_colour, hidden_abilities = :hidden_abilities WHERE oid = "
            ":oid",
            {
                'number': self.dex.text(),
                'name': self.name.text(),
                'height': self.height.text(),
                'weight': self.weight.text(),
                'gender': self.gender.text(),
                'category': self.category.text(),
                'abilities_1': self.abilities1.text(),
                'abilities_2': self.abilities2.text(),
                'type_1': self.type1.text(),
                'type_2': self.type2.text(),
                'weaknesses_1': self.weaknesses1.text(),
                'weaknesses_2': self.weaknesses2.text(),
                'weaknesses_3': self.weaknesses3.text(),
                'weaknesses_4': self.weaknesses4.text(),
                'weaknesses_5': self.weaknesses5.text(),
                'weaknesses_6': self.weaknesses6.text(),
                'weaknesses_7': self.weaknesses7.text(),
                'kanto': self.kanto.text(),
                'johto': self.johto.text(),
                'hoenn': self.hoenn.text(),
                'sinnoh': self.sinnoh.text(),
                'unova': self.unova.text(),
                'kalos': self.kalos.text(),
                'alola': self.alola.text(),
                'galar': self.galar.text(),
                'hp': self.hp.text(),
                'attack': self.attack.text(),
                'defense': self.defence.text(),
                'special_attack': self.spattack.text(),
                'special_defense': self.spdefence.text(),
                'speed': self.speed.text(),
                'evolutions_1': self.evolutions1.text(),
                'evolutions_2': self.evolutions2.text(),
                'evolutions_3': self.evolutions3.text(),
                'evolutions_4': self.evolutions4.text(),
                'evolutions_5': self.evolutions5.text(),
                'evolutions_6': self.evolutions6.text(),
                'evolutions_7': self.evolutions7.text(),
                'evolutions_8': self.evolutions8.text(),
                'evolutions_9': self.evolutions9.text(),
                'mega': self.mega.text(),
                'gigantamax': self.giga.text(),
                'alolan': self.alolan.text(),
                'galarian': self.galarian.text(),
                'bio': self.plainTextEdit.toPlainText(),
                'generation': self.generation.text(),
                'pokedex_colour': self.pcolour.text(),
                'hidden_abilities': self.habilities.text(),
                'oid': temp_id
            })

        # updated successfully message
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Information)
        msgBox.setText("Pokemon: " + temp_id + " Updated Successfully!")
        msgBox.setWindowTitle("Pokemon Update!:")
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBox.exec_()

        conn.commit()
        conn.close()
        Ui_Pokedex.clear_all(self)

    def about(self):
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Information)
        msgBox.setWindowTitle("About Pokédex")
        msgBox.setText("This Application was written in Python3 and PyQt5" "\n" "I created this Pokedex as an "
                       "exercise to learn PYQT! I am also a big fan of Pokémon :)" "\n\n" "All the code is open source "
                       "and is licenced under the GPLv2 ")
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBox.exec_()

    def dbtocsv(self):
        conn = sqlite3.connect('pokedex.db')
        c = conn.cursor()
        c.execute('SELECT * FROM pokemon')
        with open('pokedex.csv', 'w', encoding='utf8') as out_csv_file:
            csv_out = csv.writer(out_csv_file)
            # write header
            csv_out.writerow([d[0] for d in c.description])
            # write data
            for result in c:
                csv_out.writerow(result)
        conn.close()
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Information)
        msgBox.setWindowTitle("Export to CSV")
        msgBox.setText("Export Complete please see root directory!")
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBox.exec_()
        return


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Pokedex = QtWidgets.QMainWindow()
    ui = Ui_Pokedex()
    ui.setupUi(Pokedex)
    Pokedex.show()
    sys.exit(app.exec_())
